# Git test

#### 一、创建本地仓库
###### 1.创建本地代码库

```
# 在当前目录新建一个Git代码库
git init

# 新建一个目录，将其初始化为Git代码库
git init [project-name]
```
###### 2.克隆
```
git clone [url]
```
#### 二、增加、删除或移动文件
###### 1.增加
```
# 添加未追踪文件
git add file-name

# 添加所有未追踪文件
git add -A

# 添加并且提交所有未追踪文件
git commit -a
```
###### 2.删除
```
# 删除工作区文件，并且将这次删除放入暂存区
git rm [file]...

# 如果删除之前修改过并且已经放到暂存区域的话，则必须要用强制删除选项 -f
git rm -f <file>

# 停止追踪指定文件，但该文件会保留在工作区
git rm --cached [file]
```
###### 3.移动
```
# 改名文件 并且将其放入暂存区
git mv [file-original] [file-renamed]
```

#### 三、提交
```
# 提交并添加注释
git add [file]
git commit -m '注释'

# 提交全部
git commit -a

# 跳过add文件提交并且注释
git commit -am '注释'
```

#### 四、同步
1. 下载远程仓库所有变动
```
git fetch [remote]
```
2. 拉去远程仓库变化
```
# 取回远程仓库的变化，并与本地分支合并
$ git pull [remote] [branch]
```
3. 上传本地分支到远程仓库
```
 上传本地指定分支到远程仓库
git push [remote] [branch]

 强行推送当前分支到远程仓库，即使有冲突
git push [remote] --force

 推送所有分支到远程仓库
git push [remote] --all
```
4. 同步分支本地分支到远程
```
同步分支本地分支到远程
git remote show origin
删除本地多余分支
git branch -D feature/chatfix
```

#### 五、查看
1. 查看git状态
```
git status查看在你上次提交之后是否有修改
git status -s

 执行 git diff 来查看执行 git status 的结果的详细信息 
```
2. 查看提交历史
```
git log
```

#### 六、分支
1. 创建分支
```
git branch [branchname]
```
2. checkout 切换指定分支
```
git checkout [branchname]
```
3. 删除分支
```
git branch -d [branchname]
```
4. 合并分支
```
指定的分支合并到当前分支中去
git merge [brench]
```
	* step1 切换到主分支
```
git checkout master
```
	* step2，把指定的分支合并到当前分支（主分支）
```
git merge change_branch
当把辅分支合并到 "master" 分支时，如果出现合并冲突，接下来就需要手动去修改它
```
	* step3，提交合并
```
在 Git 中，我们可以用 git add 要告诉 Git 文件冲突已经解决，并使用git commit来提交分支的合并。
git add solve_conflict
git commit
```